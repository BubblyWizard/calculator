package edu.hawaii.ics211;

/**
 * Test class for the calculator. Tests every method using various integers to be thorough.
 * @author Christian Leandro
 *
 */
public class TestCalculator {

	public static void main(String[] args) {
		
		Calculator myCalculator = new Calculator();
		System.out.println("1 + 0 = "+myCalculator.add(1, 0));
		System.out.println("(-1) + 5 = "+myCalculator.add(-1, 5));
		System.out.println("10 - 1 = "+myCalculator.subtract(10, 1));
		System.out.println("10 - (-1) = "+myCalculator.subtract(10, -1));
		System.out.println("2 * 2 = "+myCalculator.multiply(2, 2));
		System.out.println("5 * 0 = "+myCalculator.multiply(5, 0));
		System.out.println("5 * (-2) "+myCalculator.multiply(5, -2));
		System.out.println("4 / 2 = "+myCalculator.divide(4, 2));
		System.out.println("0 / 5 = "+myCalculator.divide(0, 5));
		System.out.println("100 / -10 = "+myCalculator.divide(100, -10));
		System.out.println("4 % 2 = "+myCalculator.modulo(4, 2));
		System.out.println("5 % 3 = "+myCalculator.modulo(5, 3));
		System.out.println("5^0 = "+myCalculator.pow(5, 0));
		System.out.println("5^1 = "+myCalculator.pow(5, 1));
		System.out.println("(-1)^2 = "+myCalculator.pow(-1, 2));
		System.out.println("3^3 = "+myCalculator.pow(3, 3));
	}

}
